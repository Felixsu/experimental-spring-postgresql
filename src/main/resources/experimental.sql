-- Sequence: public.seq_users

-- DROP SEQUENCE public.seq_users;

CREATE SEQUENCE public.seq_users
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 999999
  START 10
  CACHE 1;
ALTER TABLE public.seq_users
  OWNER TO postgres;

-- Table: public.users

-- DROP TABLE public.users;

CREATE TABLE public.users
(
  id integer NOT NULL DEFAULT nextval('seq_users'::regclass),
  email character varying(255) NOT NULL,
  first_name character varying(255) NOT NULL,
  last_name character varying(255) NOT NULL,
  created_at timestamp with time zone,
  updated_at timestamp with time zone,
  deleted_at timestamp with time zone,
  CONSTRAINT users_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.users
  OWNER TO postgres;

-- Index: public.idx_users_id

-- DROP INDEX public.idx_users_id;

CREATE UNIQUE INDEX idx_users_id
  ON public.users
  USING btree
  (id);
