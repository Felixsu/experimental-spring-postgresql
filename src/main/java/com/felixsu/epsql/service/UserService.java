package com.felixsu.epsql.service;

import com.felixsu.epsql.base.service.BaseEntityService;
import com.felixsu.epsql.entity.User;
import com.felixsu.epsql.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * created on 20/04/2017.
 *
 * @author felixsu
 */
@Service
public class UserService extends BaseEntityService<User, Integer, UserRepository> {

    @Autowired
    public UserService(UserRepository dao) {
        super(dao, User.class);
    }
}
