package com.felixsu.epsql.common.model;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * created on 20/04/2017.
 *
 * @author felixsu
 */
public class Error {
    private int code;
    private String message;
    private String stackTrace;

    public static Error fromThrowable(int code, Throwable t) {
        StringWriter sw = new StringWriter();
        t.printStackTrace(new PrintWriter(sw));
        return new Error(code, t.getMessage(), sw.toString());
    }

    public Error() {
    }

    public Error(int code, String message, String stackTrace) {
        this.code = code;
        this.message = message;
        this.stackTrace = stackTrace;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }
}
