package com.felixsu.epsql.common.advice;

import com.felixsu.epsql.common.exception.BadRequestException;
import com.felixsu.epsql.common.exception.MultipleChoicesException;
import com.felixsu.epsql.common.exception.NotFoundException;
import com.felixsu.epsql.common.model.Error;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class EntityControllerAdvice {

    private static final int STATUS_MULTIPLE_CHOICES = 300;
    private static final int STATUS_BAD_REQUEST = 400;
    private static final int STATUS_NOT_FOUND = 404;
    private static final int STATUS_INTERNAL_SERVER_ERROR = 500;

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    Error entityBadRequestExceptionHandler(BadRequestException e) {
        return Error.fromThrowable(STATUS_BAD_REQUEST, e);
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    Error entityNotFoundExceptionHandler(NotFoundException e) {
        return Error.fromThrowable(STATUS_NOT_FOUND, e);
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    Error entityInternalErrorExceptionHandler(RuntimeException e) {
        return Error.fromThrowable(STATUS_INTERNAL_SERVER_ERROR, e);
    }

    @ExceptionHandler(MultipleChoicesException.class)
    @ResponseStatus(HttpStatus.MULTIPLE_CHOICES)
    Error entityMultipleChoicesExceptionHandler(MultipleChoicesException e) {
        return Error.fromThrowable(STATUS_MULTIPLE_CHOICES, e);
    }
}