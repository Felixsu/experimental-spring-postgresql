package com.felixsu.epsql.common.exception;

public class MultipleChoicesException extends Exception {

    public MultipleChoicesException(String message) {
        super(message);
    }

    public MultipleChoicesException(String message, Throwable cause) {
        super(message, cause);
    }

    public MultipleChoicesException(Throwable cause) {
        super(cause);
    }
}
