package com.felixsu.epsql.common;

import com.felixsu.epsql.entity.AuditableEntity;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 4/21/17.
 *
 * @author felixsoewito
 */
public class PredicateBuilder<T> {
    private List<Predicate> predicates = new ArrayList<>();
    private Root<T> root;
    private CriteriaQuery query;
    private CriteriaBuilder builder;

    public static <T> PredicateBuilder<T> getDefaultBuilder(Root<T> root, CriteriaQuery query, CriteriaBuilder builder) {
        PredicateBuilder<T> b = new PredicateBuilder<>(root, query, builder);
        b.addIsNull(AuditableEntity.DELETED_AT);
        return b;
    }

    public PredicateBuilder(Root<T> root, CriteriaQuery query, CriteriaBuilder builder) {
        this.root = root;
        this.query = query;
        this.builder = builder;
    }

    public void addEqualInteger(String property, Integer value) {
        if (value != null) {
            predicates.add(builder.equal(root.get(property), value));
        }
    }

    public void addEqualString(String property, String value) {
        if (value != null) {
            predicates.add(builder.equal(root.get(property), value));
        }
    }

    public void addEqualBoolean(String property, Boolean value) {
        if (value != null) {
            predicates.add(builder.equal(root.get(property), value));
        }
    }

    public void addIsNull(String property) {
        predicates.add(builder.isNull(root.get(property)));
    }


    public Predicate getAndPredicate() {
        Predicate result = builder.and(predicates.toArray(new Predicate[predicates.size()]));
        return result;
    }


}
