package com.felixsu.epsql.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created on 20/04/2017.
 *
 * @author felixsu
 */
@RestController
public class IndexController {

    @RequestMapping
    public String index() {
        return "ready to go..";
    }
}
