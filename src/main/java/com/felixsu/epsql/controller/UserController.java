package com.felixsu.epsql.controller;

import com.felixsu.epsql.base.controller.BaseEntityController;
import com.felixsu.epsql.entity.User;
import com.felixsu.epsql.repository.UserRepository;
import com.felixsu.epsql.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * created on 21/04/2017.
 *
 * @author felixsu
 */
@RestController
@RequestMapping(path = "user")
public class UserController extends BaseEntityController<User, Integer, UserRepository> {

    @Autowired
    public UserController(UserService service) {
        super(service);
    }
}
