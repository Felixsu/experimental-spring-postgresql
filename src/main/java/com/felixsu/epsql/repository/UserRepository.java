package com.felixsu.epsql.repository;

import com.felixsu.epsql.base.BaseRepository;
import com.felixsu.epsql.entity.User;
import org.springframework.data.repository.CrudRepository;

/**
 * @author felixsu on 19/04/2017.
 */
public interface UserRepository extends BaseRepository<User, Integer> {
}
