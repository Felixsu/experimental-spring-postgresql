package com.felixsu.epsql.helper;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

/**
 * Created on 4/24/17.
 *
 * @author felixsoewito
 */
public class ReflectionHelper {

    public static Set<String> getFields(Class clazz) {
        Set<String> result = new HashSet<>();
        for (Field field : clazz.getDeclaredFields()) {
            result.add(field.getName());
        }
        return result;
    }

    public static Class getTypeOfField(String fieldName, Class clazz) throws NoSuchFieldException {
        Field field = clazz.getDeclaredField(fieldName);
        field.setAccessible(true);
        return field.getType();
    }
}
