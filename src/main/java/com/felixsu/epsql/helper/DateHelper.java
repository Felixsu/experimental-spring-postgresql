package com.felixsu.epsql.helper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created on 9/6/16.
 *
 * @author felixsoewito
 */
public class DateHelper {
    private static final String ISO_8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private static final SimpleDateFormat ISO_8601_DATE_FORMATTER = new SimpleDateFormat(ISO_8601_FORMAT);

    public static SimpleDateFormat getIso8601DateFormatterInstance(){
        return ISO_8601_DATE_FORMATTER;
    }

    public static String getIso8601DateFormatterString(Date d){
        return ISO_8601_DATE_FORMATTER.format(d);
    }

    public static String getIso8601DateFormatterString(){
        return getIso8601DateFormatterString(Calendar.getInstance().getTime());
    }

    public static long getTime(){
        return Calendar.getInstance().getTimeInMillis();
    }
}
