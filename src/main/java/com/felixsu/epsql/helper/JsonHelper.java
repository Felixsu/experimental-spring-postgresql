package com.felixsu.epsql.helper;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created on 4/22/17.
 *
 * @author felixsoewito
 */
public class JsonHelper {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static ObjectMapper getInstance() {
        return OBJECT_MAPPER;
    }

}
