package com.felixsu.epsql.base.controller;

import com.felixsu.epsql.common.exception.NotFoundException;
import com.felixsu.epsql.base.service.BaseEntityService;
import com.felixsu.epsql.base.BaseRepository;
import com.felixsu.epsql.common.model.PagingWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.List;

/**
 * created on 20/04/2017.
 *
 * @author felixsu
 */
public class BaseEntityController<T, ID extends Serializable, S extends BaseRepository<T, ID>> {

    protected BaseEntityService<T, ID, S> service;

    @Autowired
    protected HttpServletRequest request;

    public BaseEntityController(BaseEntityService<T, ID, S> service) {
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody T model) {
        return new ResponseEntity<>(service.create(model), HttpStatus.OK);
    }

    @RequestMapping(
            method = RequestMethod.PUT,
            value = "{id}")
    public ResponseEntity update(@PathVariable ID id, @RequestBody T model) throws NotFoundException {
        return new ResponseEntity<>(service.update(id, model), HttpStatus.OK);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "{id}"
    )
    public ResponseEntity get(@PathVariable ID id) throws NotFoundException {
        return new ResponseEntity<>(service.get(id), HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(
            method = RequestMethod.GET
    )
    public ResponseEntity getAll(Pageable pageable) {
        List<T> result = service.getAll(request.getParameterMap(), pageable);
        return new ResponseEntity<>(PagingWrapper.wrap((PageImpl<T>) result.get(0)), HttpStatus.OK);
    }

    @RequestMapping(
            method = RequestMethod.DELETE,
            path = "{id}"
    )
    public ResponseEntity delete(@PathVariable ID id) throws NotFoundException {
        service.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
