package com.felixsu.epsql.base.service;

import com.felixsu.epsql.base.BaseRepository;
import com.felixsu.epsql.common.PredicateBuilder;
import com.felixsu.epsql.common.exception.NotFoundException;
import com.felixsu.epsql.helper.ReflectionHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * created on 20/04/2017.
 *
 * @author felixsu
 */
public class BaseEntityService<T, ID extends Serializable, S extends BaseRepository<T, ID>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseEntityService.class.getName());

    private static final String DATE_FROM = "date_from";
    private static final String DATE_TO = "date_to";

    protected final S dao;
    private final Class entityClass;

    public BaseEntityService(S dao, Class entityClass) {
        this.dao = dao;
        this.entityClass = entityClass;
    }

    public T create(T model){
        return dao.save(model);
    }

    public T update(ID id, T model) throws NotFoundException {
        if (!dao.exists(id)) {
            throw new NotFoundException("unique identifier not found: " + id);
        }
        return dao.save(model);
    }

    public T get(ID id) throws NotFoundException {
        T result = dao.findOne(id);
        if (result == null) {
            throw new NotFoundException("unique identifier not found: " + id);
        }
        return result;
    }

    public List<T> getAll(Map<String, String[]> queryStrings, Pageable pageable) {
        final Map<String, String> m = queryStrings.entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        e -> e.getValue().length > 0 ? e.getValue()[0] : null
                ));

        return dao.findAll((root, query, cb) -> {
            PredicateBuilder<T> builder = PredicateBuilder.getDefaultBuilder(root, query, cb);
            final Set<String> fieldNames = ReflectionHelper.getFields(entityClass);
            for (Map.Entry<String, String> entry : m.entrySet()) {
                final String key = entry.getKey();
                if (fieldNames.contains(key)) {
                    try {
                        Class clazz = ReflectionHelper.getTypeOfField(key, entityClass);
                        if (clazz.isAssignableFrom(String.class)) {
                            builder.addEqualString(key, entry.getValue());
                        } else if (clazz.isAssignableFrom(Integer.class)) {
                            Integer val = Integer.parseInt(entry.getValue());
                            builder.addEqualInteger(key, val);
                        } else if (clazz.isAssignableFrom(Double.class)) {

                        } else if (clazz.isAssignableFrom(Boolean.class)) {
                            Boolean val = Boolean.parseBoolean(entry.getValue());
                            builder.addEqualBoolean(key, val);
                        }
                    } catch (Exception e) {
                        LOGGER.warn(e.getMessage(), e);
                    }
                }
            }
            return builder.getAndPredicate();
        }, pageable == null ? new PageRequest(0, 20) : pageable);
    }

    public void delete(ID id) throws NotFoundException {
        if (!dao.exists(id)) {
            throw new NotFoundException("unique identifier not found: " + id);
        }
        dao.softDelete(id, Calendar.getInstance().getTime());
    }
}
