package com.felixsu.epsql.base;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by felixsu on 19/04/2017.
 */
@NoRepositoryBean
public interface BaseRepository<T, ID extends Serializable> extends JpaRepository<T, ID> {
    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e.deletedAt IS NULL")
    List<T> findAll();

    List<T> findAll(Specification<T> specification, Pageable page);

    @Override
    @Query("SELECT e FROM #{#entityName} e WHERE e.id = ?1 AND e.deletedAt IS NULL")
    T findOne(ID id);

    //Look up deleted entities
    @Query("SELECT e FROM #{#entityName} e WHERE e.deletedAt IS NULL")
    List<T> recycleBin();

    //Soft delete.
    @Query("UPDATE #{#entityName} e SET e.deletedAt = ?2 WHERE e.id = ?1")
    @Transactional
    @Modifying
    void softDelete(ID id, Date date);
}
